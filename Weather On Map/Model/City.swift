//
//  City.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class City: Object, Mappable, Copyable, Identified {

    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var latitude: Double = 0
    @objc dynamic var longitude: Double = 0
    @objc dynamic var temp: Double = Constants.zeroCelsius

    required convenience init?(json: JSON) {
        self.init()
        
        guard
            let id = json["id"].int,
            let name = json["name"].string,
            let latitude = json["coord"]["lat"].double,
            let longitude = json["coord"]["lon"].double,
            let temp = json["main"]["temp"].double
        else {
            return nil
        }
        
        self.id = id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.temp = temp
    }
    
    required convenience init(instance: City) {
        self.init()
        self.id = instance.id
        self.name = instance.name
        self.latitude = instance.latitude
        self.longitude = instance.longitude
        self.temp = instance.temp
    }
}

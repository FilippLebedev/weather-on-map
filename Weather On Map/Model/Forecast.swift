//
//  Forecast.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import SwiftyJSON

class Forecast: Mappable {
    
    var temp: Double = Constants.zeroCelsius
    var date = Date()
    
    required convenience init?(json: JSON) {
        self.init()
        
        guard
            let temp = json["main"]["temp"].double,
            let dateString = json["dt_txt"].string
            else { return }
        
        self.temp = temp
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: dateString) {
            self.date = date
        }
    }
}

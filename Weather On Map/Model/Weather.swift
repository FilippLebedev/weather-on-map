//
//  Weather.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import SwiftyJSON

class Weather: Mappable {
    
    var name: String = ""
    var temp: Double = Constants.zeroCelsius
    
    required convenience init?(json: JSON) {
        self.init()
        
        guard
            let temp = json["main"]["temp"].double,
            let name = json["name"].string
        else { return }
        
        self.temp = temp
        self.name = name
    }
}

//
//  CitiesViewController.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit
import RealmSwift

class CitiesViewController: UIViewController, GlobalRouterUser {

    @IBOutlet weak var tableView: UITableView!
    
    let cityProvider = CityProvider()
    
    var cities: [City] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero)
        cityProvider.delegate = self
    }
}

extension CitiesViewController: CityProviderDelegate {
    
    func receiveCityResults(_ results: Results<City>) {
        cities = Array(results)
        tableView.reloadData()
    }
}

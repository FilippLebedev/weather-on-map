//
//  CityTableViewCell.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet weak private var nameLabel: UILabel!
    
    var name: String = "" {
        didSet {
            nameLabel.text = name
        }
    }
}

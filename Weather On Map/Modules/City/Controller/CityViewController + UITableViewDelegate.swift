//
//  CityViewController + UITableViewDelegate.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 23.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit

extension CityViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellForecast", for: indexPath) as? ForecastTableViewCell else { return UITableViewCell() }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ForecastTableViewCell else { return }
        
        let date = forecastData.dateOfGroup(index: indexPath.row)
        cell.date = date
        cell.temp = (minimal: forecastData.minimalTemp(for: date) ?? Constants.zeroCelsius, maximal: forecastData.maximalTemp(for: date) ?? Constants.zeroCelsius)
    }
}

//
//  CityViewController.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {

    @IBOutlet weak var tempLabel: TemperatureLabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let apiClient = APIClient()
    let forecastData = ForecastDataSource()
    
    var cityId: Int = 0 {
        didSet {
            apiClient.getWeather(id: cityId) { (result) in
                guard let result = result else { return }
                self.weather = result
            }
            
            apiClient.getForecast(id: cityId) { (result) in
                self.forecastData.assignData(result)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    var weather: Weather? = nil {
        didSet {
            DispatchQueue.main.async {
                self.cityNameLabel.isHidden = false
                self.cityNameLabel.text = self.weather?.name
                self.tempLabel.value = self.weather?.temp ?? Constants.zeroCelsius
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero)
    }
}

//
//  ForecastDataSource.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class ForecastDataSource {

    private var forecastGroups: [ForecastGroup] = []
    
    var count: Int {
        return forecastGroups.count
    }
    
    func assignData(_ data: [Forecast]) {
        forecastGroups = []
        
        let sortedData = data.sorted(by: { $0.date < $1.date })
        
        var prevDay: Int = 0
        
        for (index, obj) in sortedData.enumerated() {
            if index == 0 {
                prevDay = Calendar.current.component(.day, from: obj.date)
                let newGroup = ForecastGroup(date: obj.date)
                newGroup.forecasts.append(obj)
                forecastGroups.append(newGroup)
            } else {
                prevDay = Calendar.current.component(.day, from: data[index - 1].date)
                if Calendar.current.component(.day, from: obj.date) != prevDay {
                    let newGroup = ForecastGroup(date: obj.date)
                    newGroup.forecasts.append(obj)
                    forecastGroups.append(newGroup)
                } else {
                    forecastGroups.last?.forecasts.append(obj)
                }
            }
        }
    }
    
    func group(of date: Date) -> ForecastGroup? {
        for group in forecastGroups {
            if group.date == date {
                return group
            }
        }
        return nil
    }
    
    func minimalTemp(for date: Date) -> Double? {
        if let group = group(of: date) {
            return group.minimalTemp
        }
        return nil
    }
    
    func maximalTemp(for date: Date) -> Double? {
        if let group = group(of: date) {
            return group.maximalTemp
        }
        return nil
    }
    
    func dateOfGroup(index: Int) -> Date {
        return forecastGroups[index].date
    }
    
    func minimalTemp(index: Int) -> Double? {
        return forecastGroups[index].minimalTemp
    }
    
    func maximalTemp(index: Int) -> Double? {
        return forecastGroups[index].maximalTemp
    }
}

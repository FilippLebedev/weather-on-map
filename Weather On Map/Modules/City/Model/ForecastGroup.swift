//
//  ForecastGroup.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class ForecastGroup {
    
    var forecasts: [Forecast] = []
    var date = Date()
    
    var minimalTemp: Double {
        if forecasts.count > 1 {
            return forecasts.sorted(by: { $0.temp < $1.temp })[0].temp
        } else if forecasts.count == 1 {
            return forecasts[0].temp
        }
        return Constants.zeroCelsius
    }
    
    var maximalTemp: Double {
        if forecasts.count > 1 {
            return forecasts.sorted(by: { $0.temp > $1.temp })[0].temp
        } else if forecasts.count == 1 {
            return forecasts[0].temp
        }
        return Constants.zeroCelsius
    }
    
    init(date: Date) {
        self.date = date
    }
    
    init(date: Date, data: [Forecast]) {
        self.date = date
        forecasts = data
    }
}

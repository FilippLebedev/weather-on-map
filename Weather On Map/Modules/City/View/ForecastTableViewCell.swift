//
//  ForecastTableViewCell.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak private var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    var date = Date() {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateLabel.text = dateFormatter.string(from: date)
        }
    }
    
    var temp: (minimal: Double, maximal: Double) = (Constants.zeroCelsius, Constants.zeroCelsius) {
        didSet {
            tempLabel.text = "\(Int(temp.minimal.celsius)) .. \(Int(temp.maximal.celsius))"
        }
    }
}

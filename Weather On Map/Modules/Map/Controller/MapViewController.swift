//
//  MapViewController.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import RealmSwift

class MapViewController: UIViewController, GlobalRouterUser {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let apiClient = APIClient()
    let initialDataProvider = InitialDataProvider()
    let cityProvider = CityProvider()
    
    var cities: [City] = []
    
    override func viewDidLoad() {
        mapView.delegate = self
        cityProvider.delegate = self
    }
    
    func updateAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
        
        for (index, city) in cities.enumerated() {
            let ann = CityAnnotation(cityId: city.id, name: city.name, coordinate: CLLocationCoordinate2D(latitude: city.latitude, longitude: city.longitude))
            mapView.addAnnotation(ann)
            
            // Request actual temperature
            
            apiClient.getWeather(id: ann.cityId) { (result) in
                guard let result = result else { return }
                ann.temp = result.temp
            }
            
            if index == cities.count - 1 {
                mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: city.latitude, longitude: city.longitude), span: MKCoordinateSpan(latitudeDelta: 5, longitudeDelta: 5)), animated: true)
            }
        }
    }
    
    @IBAction func citiesButtonPressed(_ sender: Any) {
        pushCities(self)
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        pushSearch(self)
    }
}

extension MapViewController: CityProviderDelegate {
    
    func receiveCityResults(_ results: Results<City>) {
        cities = Array(results)
        updateAnnotations()
        
        if cities.count != 0 {
            initialDataProvider.deactivate()
        } else {
            initialDataProvider.setInitialData()
        }
    }
}

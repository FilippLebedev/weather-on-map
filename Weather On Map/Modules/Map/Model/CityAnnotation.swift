//
//  CityAnnotation.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 22.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import MapKit

class CityAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    let coordinate: CLLocationCoordinate2D
    
    let cityId: Int
    let name: String
    
    var temp: Double = Constants.zeroCelsius {
        didSet {
            self.title = "\(name) (\(Int(temp.celsius))℃)"
        }
    }
    
    init(cityId: Int, name: String, coordinate: CLLocationCoordinate2D) {
        self.cityId = cityId
        self.name = name
        self.title = "\(name) (...)"
        self.coordinate = coordinate
        super.init()
    }
}

//
//  SearchViewController + UITableViewDelegate.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 23.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit
import Alertift

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellFoundCity", for: indexPath) as? FoundCityTableViewCell else { return UITableViewCell() }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? FoundCityTableViewCell else { return }
        
        let city = cities[indexPath.row]
        
        cell.name = city.name
        cell.coordinates = (latitude: city.latitude, longitude: city.longitude)
        cell.temp = city.temp
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = cities[indexPath.row]
        
        RealmService.shared.isObjectExists(city) { [weak self] (result) in
            guard let _self = self else { return }
            
            DispatchQueue.main.async {
                if result {
                    Alertift.alert(title: "Can not append", message: "\(city.name) already exists (ID: \(city.id))")
                        .action(.default("OK"))
                        .show()
                } else {
                    Alertift.alert(title: "City appending", message: "Would you like to add \(city.name)?")
                        .action(.default("Yes")) { _, _, _ in
                            _self.addCity(city: city)
                        }
                        .action(.cancel("Cancel"))
                        .show()
                }
            }
        }
    }
}

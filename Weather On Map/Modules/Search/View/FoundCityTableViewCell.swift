//
//  FoundCityTableViewCell.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class FoundCityTableViewCell: UITableViewCell {

    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var coordinatesLabel: UILabel!
    @IBOutlet weak private var tempLabel: TemperatureLabel!
    
    var name: String = "" {
        didSet {
            nameLabel.text = name
        }
    }
    
    var temp: Double = Constants.zeroCelsius {
        didSet {
            tempLabel.value = temp
        }
    }
    
    var coordinates: (latitude: Double, longitude: Double) = (0, 0) {
        didSet {
            coordinatesLabel.text = "Latitude: \(coordinates.latitude) / Longitude: \(coordinates.longitude)"
        }
    }
}

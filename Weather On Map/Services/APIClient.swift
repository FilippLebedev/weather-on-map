//
//  APIClient.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

final class APIClient {
    
    let mapper = Mapper()
    
    func searchCity(_ name: String, completion: @escaping ([City]) -> Void) {
        var data: [City] = []
        let query = CitiesSearchRequest(query: name)
        
        query.send() { (result, error) in
            guard error == nil else {
                print(error?.localizedDescription ?? "Unknown error")
                completion([])
                return
            }
            
            guard let result = result else {
                completion([])
                return
            }
            
            let resultList = result["list"]
            data = self.mapper.map(data: resultList, type: City.self) as? [City] ?? []
            completion(data)
        }
    }
    
    func getWeather(id: Int, completion: @escaping (Weather?) -> Void) {
        var data: [Weather] = []
        let query = CityWeatherCurrentGetRequest(id: id)
        
        query.send() { (result, error) in
            guard error == nil else {
                print(error?.localizedDescription ?? "Unknown error")
                completion(nil)
                return
            }
            
            guard let result = result else {
                completion(nil)
                return
            }
            
            data = self.mapper.map(data: result, type: Weather.self) as? [Weather] ?? []
            
            guard data.count > 0 else {
                completion(nil)
                return
            }
            
            let element = data[0]
            completion(element)
        }
    }
    
    func getForecast(id: Int, completion: @escaping ([Forecast]) -> Void) {
        var data: [Forecast] = []
        let query = CityWeatherForecastGetRequest(id: id)
        
        query.send() { (result, error) in
            guard error == nil else {
                print(error?.localizedDescription ?? "Unknown error")
                completion([])
                return
            }
            
            guard let result = result else {
                completion([])
                return
            }
            
            let resultList = result["list"]
            data = self.mapper.map(data: resultList, type: Forecast.self) as? [Forecast] ?? []
            completion(data)
        }
    }
}

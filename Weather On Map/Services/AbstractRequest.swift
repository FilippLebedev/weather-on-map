//
//  AbstractRequest.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import SwiftyJSON

class AbstractRequest {
    
    var path: String
    
    init(path: String) {
        self.path = path
    }
    
    func send(completion: @escaping (JSON?, Error?) -> Void) {
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        if let url = URL(string: preparedPath(path)) {
            session.dataTask(with: url) { (data, response, error) in
                completion(JSON(data), error)
            }.resume()
        } else {
            print("Incorrect URL: \(path)")
            completion(nil, nil)
        }
    }
    
    func preparedPath(_ path: String) -> String {
        return path.replacingOccurrences(of: " ", with: "%20")
    }
}

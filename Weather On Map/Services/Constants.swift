//
//  Constants.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

final class Constants {
    
    private init() {}
    
    static let apiKey = "2dd7106ed103d772db350a7c27c4c0dd"
    static let zeroCelsius: Double = 273
}

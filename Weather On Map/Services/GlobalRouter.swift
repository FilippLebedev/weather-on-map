//
//  GlobalRouter.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 23.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit

protocol GlobalRouterUser {
    func pushCities(_ vc: UIViewController)
    func pushSearch(_ vc: UIViewController)
    func pushCity(_ vc: UIViewController, cityId: Int)
}

extension GlobalRouterUser {
    
    func pushCities(_ vc: UIViewController) {
        vc.navigationController?.pushViewController(viewController("CitiesViewController"), animated: true)
    }
    
    func pushSearch(_ vc: UIViewController) {
        vc.navigationController?.pushViewController(viewController("SearchViewController"), animated: true)
    }
    
    func pushCity(_ vc: UIViewController, cityId: Int) {
        if let controller = viewController("CityViewController") as? CityViewController {
            vc.navigationController?.pushViewController(controller, animated: true)
            controller.cityId = cityId
        }
    }
    
    private func viewController(_ name: String) -> UIViewController {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name)
    }
}


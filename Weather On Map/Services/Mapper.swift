//
//  Mapper.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import SwiftyJSON

final class Mapper {
    
    func map<T: Mappable>(data: JSON, type: T.Type) -> [Mappable] {
        switch type {
        case is City.Type, is Forecast.Type:
            return mapArray(data: data, type: type)
        case is Weather.Type:
            return mapElement(data: data, type: type)
        default:
            return []
        }
    }
    
    private func mapArray<T: Mappable>(data: JSON, type: T.Type) -> [T] {
        var result: [T] = []
        
        for (index, object) in data.enumerated() {
            if let newObject = T(json: data[index]) {
                result.append(newObject)
            }
        }
        
        return result
    }
    
    private func mapElement<T: Mappable>(data: JSON, type: T.Type) -> [T] {
        var result: [T] = []
        
        if let newObject = T(json: data) {
            result.append(newObject)
        }
        
        return result
    }
}

//
//  CityProvider.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 23.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import RealmSwift

protocol CityProviderDelegate: class {
    func receiveCityResults(_ results: Results<City>)
}

class CityProvider {
    
    let storage = Storage()
    var notificationToken: NotificationToken? = nil
    
    weak var delegate: CityProviderDelegate? = nil {
        didSet {
            setup()
        }
    }
    
    func setup() {
        notificationToken = storage.localCityResults.observe() { [weak self] (changes: RealmCollectionChange<Results<City>>?) in
            guard let _self = self else {return}
            guard let changes = changes else { return }
            
            switch changes {
            case .initial(let cities), .update(let cities, _, _, _):
                _self.delegate?.receiveCityResults(cities)
            case .error(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
}

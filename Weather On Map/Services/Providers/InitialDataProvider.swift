//
//  InitialDataProvider.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 23.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import SwiftyJSON

class InitialDataProvider {
    
    var isNeedToSetInitialData = true
    
    let apiClient = APIClient()
    
    func setInitialData() {
        guard isNeedToSetInitialData else { return }
        
        if let bundleURL = Bundle.main.url(forResource: "InitialData", withExtension: "bundle") {
            let fullBundle = Bundle(url: bundleURL)
            if let citiesJson = fullBundle?.path(forResource: "InitialCities", ofType: "json") {
                if let result = try? String(contentsOfFile: citiesJson, encoding: String.Encoding.utf8) {
                    let json = JSON(parseJSON: result)
                    
                    if let initCities = apiClient.mapper.map(data: json, type: City.self) as? [City] {
                        for city in initCities {
                            RealmService.shared.create(city) {}
                        }
                    }
                }
            }
        }
        
        deactivate()
    }
    
    func deactivate() {
        isNeedToSetInitialData = false
    }
}

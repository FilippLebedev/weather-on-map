//
//  RealmService.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class RealmService {

    typealias RealmApplicable = Object & Copyable & Identified
    
    static let shared = RealmService()
    
    private var operationQueueWrite = OperationQueue()
    private var dispatchQueueWrite = DispatchQueue(label: "dispatchQueueWrite", qos: .default)
    
    private init() {
        operationQueueWrite.maxConcurrentOperationCount = 1
        operationQueueWrite.underlyingQueue = dispatchQueueWrite
    }
    
    func create<T: RealmApplicable>(_ object: T, completion: @escaping () -> Void) {
        operationQueueWrite.addOperation { [weak self] in
            guard let _self = self else {
                completion()
                return
            }
            
            guard let realm = try? Realm() else {
                completion()
                return
            }
            
            try? realm.write {
                realm.add(object.duplicate())
            }
            
            try? realm.commitWrite()
            
            completion()
        }
    }
    
    func delete<T: RealmApplicable>(_ object: T, completion: @escaping () -> Void) {
        let objectId = object.id
        
        operationQueueWrite.addOperation { [weak self] in
            guard let _self = self else {
                completion()
                return
            }
            
            guard let realm = try? Realm() else {
                completion()
                return
            }
            
            let realmObject = realm.objects(T.self).filter("id = %@", objectId)
            
            if realmObject.count > 0, let objectToDelete = realmObject.first {
                try? realm.write {
                    realm.delete(objectToDelete)
                }
            }
            
            try? realm.commitWrite()
            
            completion()
        }
    }
    
    func isObjectExists<T: RealmApplicable>(_ object: T, completion: @escaping (Bool) -> Void) {
        let objectId = object.id
        
        operationQueueWrite.addOperation { [weak self] in
            guard let _self = self else {
                completion(false)
                return
            }
            
            guard let realm = try? Realm() else {
                completion(false)
                return
            }
            
            let realmObject = realm.objects(T.self).filter("id = %@", objectId)
            
            if realmObject.count > 0 {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

//
//  CitiesSearchRequest.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class CitiesSearchRequest: AbstractRequest {
    
    init(query: String) {
        let path = "http://api.openweathermap.org/data/2.5/find?q=\(query)&type=like&appid=\(Constants.apiKey)"
        super.init(path: path)
    }
}

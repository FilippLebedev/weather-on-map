//
//  CityWeatherForecastGetRequest.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class CityWeatherForecastGetRequest: AbstractRequest {
    
    init(id: Int) {
        let path = "http://api.openweathermap.org/data/2.5/forecast?id=\(id)&appid=\(Constants.apiKey)"
        super.init(path: path)
    }
}

//
//  Storage.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import RealmSwift

final class Storage {
    
    var localCityResults: Results<City> {
        let realm = try! Realm()
        let result = realm.objects(City.self)
        return result
    }
}

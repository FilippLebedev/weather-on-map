//
//  Double + Extensions.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

extension Double {
    
    var celsius: Double {
        return self - Constants.zeroCelsius
    }
}

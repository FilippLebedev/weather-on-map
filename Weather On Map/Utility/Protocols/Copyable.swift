//
//  Copyable.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 23.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

protocol Copyable {
    init(instance: Self)
}

extension Copyable {
    func duplicate() -> Self {
        return Self.init(instance: self)
    }
}

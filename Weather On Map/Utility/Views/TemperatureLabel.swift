//
//  TemperatureLabel.swift
//  Weather On Map
//
//  Created by Filipp Lebedev on 21.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class TemperatureLabel: UILabel {

    var warmColor: UIColor = .green
    var coldColor: UIColor = .blue
    
    var postfix = "℃"
    
    var value: Double = Constants.zeroCelsius {
        didSet {
            updateAppearance()
        }
    }
    
    func updateAppearance() {
        textColor = value > Constants.zeroCelsius + 5 ? warmColor : coldColor
        text = "\(Int(value.celsius))\(postfix)"
    }
}
